package com.example.demo.web;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ControllerIT {

	@Test
	public void getData() throws Exception {
		RestTemplate templ = new RestTemplate();
		ResponseEntity<String> res = templ.getForEntity("http://localhost:8080", String.class);
		assumeTrue(res.getStatusCode().equals(HttpStatus.OK));
		assumeTrue(res.getBody().equals("SUCCESS"));
	}
}
