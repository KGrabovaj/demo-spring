package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Controller {

	private String SUCCESS = "SUCCESS";
	
	@GetMapping()
	public ResponseEntity<String> getData() {
		return new ResponseEntity<String>(SUCCESS, HttpStatus.OK);
	}
}